var gulp = require('gulp'),
webserver = require('gulp-webserver');
gulp.task('serve', function () {
  return gulp.src('')
    .pipe(webserver({
      open: true,
      port: 3000,
      fallback: 'index.html'
    }));
});